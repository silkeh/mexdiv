"""Neroxis' Map Generator tooling."""

from contextlib import ExitStack
import json
import logging
import os
import os.path as path
import asyncio
import asyncio.subprocess
import re

from urllib import request
from typing import List, Dict, Optional

_DEFAULT_JAVA_FLAGS = ["-Xmx7g", "-Xms1g"]

_semaphore = asyncio.Semaphore()


class MapGenerator:
    """MapGenerator generates a SupCom map using Neroxis' map generator."""

    REPO_URL = "https://github.com/FAForever/Neroxis-Map-Generator"

    MAP_NAME_REGEX = (
        r"^neroxis_map_generator_\d+\.\d+\.\d+_[a-zA-Z0-9_]+_[a-zA-Z0-9_]+_?$"
    )

    def __init__(
        self,
        version: Optional[str] = None,
        jar_dir: str = "/tmp/neroxis",
        java_flags: Optional[List[str]] = None,
    ):
        """
        Initialize a map generator.

        :param version: Version of the map generator to use.
        :param jar_dir: Location to store the Java binaries.
        :param java_flags: Java flags to use.
        """
        if java_flags is None:
            java_flags = _DEFAULT_JAVA_FLAGS

        self.version = version
        self._jar_dir = jar_dir
        self._java_flags = java_flags

    @property
    def _jar_filename(self) -> str:
        return "NeroxisGen_{}.jar".format(self.version)

    @property
    def _jar_file(self) -> str:
        return path.join(self._jar_dir, self._jar_filename)

    @property
    def _jar_url(self) -> str:
        return "{}/releases/download/{}/{}".format(
            self.REPO_URL, self.version, self._jar_filename
        )

    def _download(self) -> None:
        if not path.exists(self._jar_dir):
            os.makedirs(self._jar_dir)

        request.urlretrieve(self._jar_url, self._jar_file)

    async def _run(self, map_name: str, map_dir: str) -> Dict[str, str]:
        if not path.exists(map_dir):
            os.makedirs(map_dir)

        if not re.match(self.MAP_NAME_REGEX, map_name):
            raise (TypeError)
        args = (
            ["java"]
            + self._java_flags
            + ["-jar", self._jar_file, "--folder-path", map_dir, "--map-name", map_name]
        )
        mapgen = await asyncio.create_subprocess_exec(
            *args, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE
        )
        stdout, stderr = await mapgen.communicate()

        err = stderr.decode()
        if err != "":
            logging.error(f'error output from mapgen: "{err}"')

        out = stdout.decode()
        data = {}
        for line in out.split("\\n"):
            if ": " in line:
                k, v = line.split(": ", 1)
                data[k] = v

        return data

    def _set_version(self, map_name):
        self.version, _ = map_name.removeprefix("neroxis_map_generator_").split("_", 1)

    def latest_version(self) -> str:
        """
        Retrieve the latest version of the map generator.

        :return: version number.
        """
        with request.urlopen(path.join(self.REPO_URL, "releases/latest")) as data:
            return json.load(data).get(".tag_name")

    @staticmethod
    def _read_from_json(json_file: str) -> Dict[str, str]:
        with open(json_file, "rb") as fd:
            return json.load(fd)

    async def generate(self, map_name: str, map_dir: str) -> Dict[str, str]:
        """
        Generate a map using a map name, and save it in the given directory.

        The map is generated to a folder identical to the map name.

        :param map_name: name of the map, eg: neroxis_map_generator_1.8.3_ob3y5qmajf6mw_ayeae_
        :param map_dir: directory to save the map in.
        :return: output values of the map generator.
        """
        if self.version is None:
            self._set_version(map_name)

        json_file = path.join(map_dir, map_name, "generate_output.json")
        if path.exists(json_file):
            return self._read_from_json(json_file)

        if not path.exists(self._jar_file):
            self._download()

        with ExitStack() as stack:
            await _semaphore.acquire()
            stack.callback(_semaphore.release)

            if path.exists(json_file):
                return self._read_from_json(json_file)

            logging.info(f'Generating map "{map_name}" in directory "{map_dir}"')
            out = await self._run(map_name, map_dir)
            with open(json_file, "w") as fp:
                json.dump(out, fp)

        return out
