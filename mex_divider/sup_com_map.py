"""Module for map formatting classes."""
import asyncio
import copy
import json
import math
import os
import types
from typing import TypeVar, Optional, Any, TextIO, Iterator

from lib.map_generator import MapGenerator
from mex_divider import PLAYER_COLORS

MapType = TypeVar("MapType", bound="Map")
SupComObjectType = TypeVar("SupComObjectType", bound="SupComObject")
MexType = TypeVar("MexType", bound="Mex")
PlayerType = TypeVar("PlayerType", bound="Player")


class SupComObject(types.SimpleNamespace):
    """Supreme Commander map objects.

    Describes generic objects on a Supreme Commander map

    Subtracting two SupComObjects returns the distance between the two objects on the map.
    """

    def __init__(self, x: float, y: float, **kwargs):
        """Instantiate a new object at a location.

        :param x: x coordinate of this object
        :param y: y coordinate of this object
        :param kwargs: Additional properties of this object
        """
        super().__init__(x=x, y=y, **kwargs)

    def distance_to(self, other: "SupComObject") -> float:
        """Get the distance between two SupcomObjects.

        :param other: Other SupComObject
        :return: The distance between the two objects on the map
        """
        if isinstance(other, SupComObject):
            return math.dist((self.x, self.y), (other.x, other.y))
        raise TypeError

    def __key(self) -> tuple[float, float]:
        return self.x, self.y

    def __hash__(self):
        """Create hash of object."""
        return hash(self.__key())

    def is_within(self, other: "SupComObject", distance: float) -> bool:
        """Check if the both objects are within range of each other are.

        :param other: SubComObject to compare with.
        :param distance: max separation distance.
        :return: true if within distance.
        """
        return self.distance_to(other) <= distance

    def location(self) -> dict[str, float]:
        """Get the location as x/y dict.

        :return: A dict with x and y coordinate
        """
        return {"x": self.x, "y": self.y}


class Player(SupComObject):
    """Supreme Commander player object."""

    def __init__(
        self,
        player_id: int,
        color: str,
        x: float,
        y: float,
        name: Optional[str] = None,
        mexes: Optional[set[MexType]] = None,
    ):
        """Instantiate a new player.

        :param player_id: Identifier of the player, must match the index in the player array
        :param color: Color of the player as string
        :param x: x coordinate of this players starting location
        :param y: y coordinate of this players starting location
        :param name: Optional name of this player
        :param mexes: Optional
        """
        self.mexes = mexes or set()
        self.color = color
        name = name or ""
        super().__init__(id=player_id, x=x, y=y, name=name)

    def __deepcopy__(self, memodict=None):
        """Get a deepcopy of the Player object.

        :param memodict: unused param
        :return: deepcopy of the Player object.
        """
        return Player(
            player_id=self.id,
            color=self.color,
            x=self.x,
            y=self.y,
            name=self.name,
            mexes=(copy.deepcopy(self.mexes)),
        )

    def as_api(self) -> dict[str, Any]:
        """Get the properties of the mex API formatted.

        :return: A dict of the properties
        """
        return {
            "id": self.id,
            "color": self.color,
            "location": self.location(),
            "name": self.name,
        }

    @classmethod
    def from_api(cls: type[PlayerType], data: dict[str, Any]) -> PlayerType:
        """Generate the Mex object from API data.

        :param data: Openapi formatted data
        :return: A Mex
        """
        name = data.get("name", None)
        return cls(
            data["id"],
            data["color"],
            data["location"]["x"],
            data["location"]["y"],
            name,
        )

    def add_mex(self, mex: MexType):
        """Assign a mex to this player.

        :param mex: The mex to assign to this player
        """
        self.mexes.add(mex)

    def remove_mex(self, mex: MexType):
        """Remove a mex to from player if it owns the mex.

        :param mex: The mex to remove from this player
        """
        self.mexes.discard(mex)

    def costs(self) -> float:
        """Calculate the costs for this player to get to all its mexes."""
        cost_sum = 0.0
        for mex in self.mexes:
            cost_sum += pow(2, 1 / self.distance_to(mex))
        return cost_sum

    def get_color(self) -> str:
        """Get the player color."""
        return self.color


class Mex(SupComObject):
    """Supreme Commander mex object."""

    def __init__(self, mex_id: int, owner: Optional[Player], x: float, y: float):
        """Instantiate a new mex.

        :param mex_id: Identifier of the mex, must match the index in the mex array
        :param owner: Owner ID of this mex
        :param x: x coordinate of this mex
        :param y: y coordinate of this mex
        """
        self.owner: Optional[Player] = owner
        super().__init__(id=mex_id, x=x, y=y)

    def as_api(self) -> dict[str, Any]:
        """Get the properties of the mex API formatted.

        :return: A dict of the properties
        """
        return {
            "id": self.id,
            "player_id": self.owner.id if self.owner else 0,
            "location": self.location(),
        }

    @classmethod
    def from_api(
        cls: type[MexType], data: dict[str, Any], players: list[Player]
    ) -> MexType:
        """Generate the Mex object from API data.

        :param data: Openapi formatted data.
        :param players: List with players.
        :return: A Mex
        """
        owner = players[data["player_id"]] if data["player_id"] is not None else None
        return cls(
            data["id"],
            owner,
            data["location"]["x"],
            data["location"]["y"],
        )

    def set_owner(self, owner: Player):
        """Set the owner for a mex.

        :param owner: New owner of the mex.
        """
        self.owner = owner

    def get_owner(self) -> Optional[Player]:
        """Get the owner of this mex.

        :return: The owner of this mex.
        """
        return self.owner


class Map:
    """Class to parse Supreme Commander maps from different formats.

    Supported formats:
    - Original Save Files
    - JSON

    :param seed: The map seed, used to localize and generate map
    :param map_directory: Directory to save the map files.
    """

    JSON_SUFFIX = "_data.json"
    PREVIEW_SUFFIX = "_preview.png"
    SAVE_SUFFIX = "_save.lua"

    def __init__(self, seed: str, map_directory: str):
        """Do Constructor method."""
        self.seed = seed
        self.map_directory = map_directory
        self.mexes: list[Mex] = []
        self.players: list[Player] = []
        self.divided: bool = False

    async def load_map(self):
        """Load map from file or generate a new map."""
        if not self._map_already_exists():
            await self._generate_map()
            return

        await self._load_from_generated_map()

    def _map_already_exists(self) -> bool:
        return os.path.exists(
            os.path.join(self.map_directory, self.seed, self.seed + self.JSON_SUFFIX)
        )

    async def _generate_map(self):
        await MapGenerator().generate(self.seed, self.map_directory)
        await self.read_from_lua()
        self.set_default_colors()

    async def _load_from_generated_map(self):
        with open(self.json_file()) as json_file:
            data = json.load(json_file)

            self.seed = data["seed"]
            self.map_dimension = data["map_dimension"]
            self.players = [
                Player.from_api(player_data) for player_data in data["players"]
            ]
            self.mexes = [
                Mex.from_api(mex_data, self.players) for mex_data in data["mexes"]
            ]
            self.divided = data["divided"]

    @property
    def mexes_per_player(self) -> int:
        """Get number of mexes per player."""
        return int(len(self.mexes) / len(self.players))

    async def read_from_lua(self):
        """Read Mex and Player information from lua save file."""
        with open(self.save_file(), "r") as f:
            for line in f:
                if "Mex" in line:
                    self._mex_from_lua(f)
                elif "ARMY" in line:
                    self._army_from_lua(f)
                elif "AREA" in line:
                    self._map_size_from_lua(f)
                elif "Armies = {" in line:
                    break

    def _mex_from_lua(self, save_file: TextIO):
        for line in save_file:
            if "position" in line:
                mex_coordinates = parse_position(line)
                self.mexes.append(
                    Mex(
                        len(self.mexes),
                        None,
                        mex_coordinates["x"],
                        mex_coordinates["z"],
                    )
                )
            if "}," in line:
                return

    def _army_from_lua(self, save_file: TextIO):
        for line in save_file:
            if "position" in line:
                army_coordinates = parse_position(line)
                self.players.append(
                    Player(
                        len(self.players),
                        "",
                        army_coordinates["x"],
                        army_coordinates["z"],
                    )
                )
            if "}," in line:
                return

    def _map_size_from_lua(self, save_file: TextIO):
        for line in save_file:
            if "rectangle" in line:
                self.map_dimension = parse_map_size(line)
                return

    def get_map_folder(self) -> str:
        """Return the folder containing the map files."""
        return os.path.join(self.map_directory, self.seed)

    def save_file(self) -> str:
        """Return the path to the save file."""
        return os.path.join(self.get_map_folder(), self.seed + self.SAVE_SUFFIX)

    def preview_file(self) -> str:
        """Return the path to the preview image."""
        return os.path.join(self.get_map_folder(), self.seed + self.PREVIEW_SUFFIX)

    def json_file(self) -> str:
        """Return the path to the json file."""
        return os.path.join(self.get_map_folder(), self.seed + self.JSON_SUFFIX)

    def save_to_json(self):
        """Save the map data to a JSON file in the map folder.

        Location of data is based on the map seed
        """
        data = {
            "seed": self.seed,
            "map_dimension": self.map_dimension,
            "mexes": [mex.as_api() for mex in self.mexes],
            "players": [player.as_api() for player in self.players],
            "divided": self.divided,
        }
        with open(self.json_file(), "w") as outfile:
            json.dump(data, outfile)
        pass

    def unassigned_mexes_iter(self) -> Iterator[Mex]:
        """Get all mexes not assigned to a player as iterator.

        :return: an iterator over the unassigned mexes
        """
        for mex in self.mexes_iter():
            if mex.owner is None:
                yield mex

    def assigned_mexes_iter(self):
        """Get all mexes assigned to a player as iterator.

        :return: an iterator over the assigned mexes
        """
        for mex in self.mexes_iter():
            if mex.owner:
                yield mex

    def mexes_iter(self):
        """Get all mexes on the map as iterator.

        :return: an iterator over all mexes.
        """
        for mex in self.mexes:
            yield mex

    def get_mex_dict_by_id(self, mex_id: int) -> dict[str, Any]:
        """Get mex dict by ID.

        :param mex_id: id of the mex
        """
        return self.mexes[mex_id].as_api()

    def get_mexes_dict(self) -> list[dict[str, Any]]:
        """Get a list of all mexes as dict."""
        return [mex.as_api() for mex in self.mexes]

    def get_player_dict_by_id(self, player_id: int) -> dict[str, Any]:
        """Get player dict by ID.

        :param player_id: id of the player
        """
        return self.players[player_id].as_api()

    def get_players_dict(self) -> list[dict[str, Any]]:
        """Get a list of all players as dict."""
        return [player.as_api() for player in self.players]

    def get_map_dict(self) -> dict[str, dict[str, float]]:
        """Get map dict with information about the map.

        :return: Dict with size and offset of the map.
        """
        return self.map_dimension

    def set_default_colors(self):
        """Set default color assigns a default color to all players.

        :return: None
        """
        for i, player in enumerate(self.players):
            player.color = rgb_to_hex(PLAYER_COLORS[i])


def rgb_to_hex(rgb: tuple[int, int, int]) -> str:
    """RGB to HEX color conversion.

    :param rgb: Tuple of RGB values in range [0, 255]
    :return: HEX formatted string usable as HTML color value
    """
    return "#{:02x}{:02x}{:02x}".format(rgb[0], rgb[1], rgb[2])


def parse_position(line: str) -> dict:
    """Parse a lua position to a dict.

    :param line: The line containing a lua position
    """
    lst = list(map(float, line.split("(")[1].split(")")[0].split(",")))
    return {"x": lst[0], "y": lst[1], "z": lst[2]}


def parse_map_size(line: str) -> dict[str, dict[str, float]]:
    """Parse a lua size to a dict.

    :param line: The line containing a lua size
    """
    lst = list(map(float, line.split("(")[1].split(")")[0].split(",")))
    return {
        "offset": {"x": lst[0], "y": lst[1]},
        "size": {"x": lst[2], "y": lst[3]},
    }


def change_mex_owner(mex: Mex, player: Player):
    """Assign a mex to a player.

    :param mex: The mex to assign
    :param player: The player to assign the mex to
    """
    previous_owner = mex.owner
    if previous_owner:
        previous_owner.remove_mex(mex)
    player.add_mex(mex)
    mex.set_owner(player)


if __name__ == "__main__":
    new_map = Map("neroxis_map_generator_1.7.1_tktdyh4g555zw_biiaeba_", "map")
    asyncio.run(new_map.load_map())
    new_map.save_to_json()
