"""Class for map drawing."""
import os
from typing import Optional

from PIL import Image, ImageDraw

from mex_divider import sup_com_map
from mex_divider.mexdiv import divide
from mex_divider.sup_com_map import SupComObject


class Map(sup_com_map.Map):
    """Class Map, children of sup_com_map.Map.

    Class to draw overlays on a Supreme Commander map.

    parent:
    """

    __doc__ += sup_com_map.Map.__doc__ or ""

    def __init__(self, seed: str, map_directory: str):
        """Create a new drawable Map.

        If the map for the seed is in the map directory,
        a new map will be generated using that seed.

        :param seed: Map seed.
        :param map_directory: Location where the map is/will be stored.
        """
        self.image: Optional[Image.Image] = None
        self.image_size: dict = {"w": 0, "h": 0}
        super().__init__(seed, map_directory)

    async def load_map(self):
        """Load map from file or generate new map."""
        await super().load_map()
        await self.load_image()

    async def load_image(self):
        """Open and store the preview of this map."""
        self.image = Image.open(self.preview_file())

    def save_image(self, filename: str):
        """Store map with overlay to file.

        :param filename: Filename for image, relative to map directory.
        """
        if not self.image:
            raise Exception("image cannot be none")
        size = (self.image.width, self.image.height)

        drawing = Image.new("RGBA", size, (0, 0, 0, 0))
        map_drawer: ImageDraw.ImageDraw = ImageDraw.Draw(drawing, "RGBA")

        self._draw_mex_overlay(map_drawer)

        filepath = os.path.join(self.get_map_folder(), filename)
        Image.alpha_composite(self.image, drawing).save(filepath)

    def _scale_pixel_from_object(self, obj: SupComObject) -> tuple[float, float]:
        """Transpose position of object on the map to a position on the canvas.

        :param obj: Object with a position that should be scaled to canvas position.
        :return: x and y position on the canvas.
        """
        return self._scale_pixel_coordinate(obj.x, obj.y)

    def _scale_pixel_coordinate(self, x: float, y: float) -> tuple[float, float]:
        """Transpose position of coordinate on the map to a position on the image.

        :param x: x position
        :param y: y position
        :return: x and y position on the canvas.
        """
        if self.image is None:
            return 0.0, 0.0
        return (
            x
            / (self.map_dimension["size"]["x"] + self.map_dimension["offset"]["x"])
            * self.image.width,
            y
            / (self.map_dimension["size"]["y"] + self.map_dimension["offset"]["y"])
            * self.image.height,
        )

    def _draw_mex_overlay(self, map_drawer: ImageDraw.ImageDraw):
        """Generate a preview with colorized mexes according to owner."""
        for draw_radius in range(50, 1, -1):
            for mex in self.assigned_mexes_iter():
                color = mex.get_owner().get_color() + "80"
                self._draw_circle(map_drawer, mex, color, draw_radius)

    def _draw_circle(
        self,
        map_drawer: ImageDraw.ImageDraw,
        obj: SupComObject,
        color: str,
        size: float,
    ):
        x, y = self._scale_pixel_from_object(obj)
        ellipse_coordinates = (x - size, y - size, x + size, y + size)
        map_drawer.ellipse(ellipse_coordinates, outline=color, fill=color, width=2)


if __name__ == "__main__":
    map_seeds = [
        "neroxis_map_generator_1.7.1_tktdyh4g555zw_biiaeba_",
        "neroxis_map_generator_1.11.0_wcxl2uccjziug_ayeae",
        "neroxis_map_generator_1.11.0_lcloxkc6xm4ru_aygae",
        "neroxis_map_generator_1.11.0_vzyw3hon7fefq_aydae",
    ]
    for map_seed in map_seeds:
        loaded_map = Map(map_seed, map_directory="map")
        divide(loaded_map)
        loaded_map.save_image("annotated.png")
