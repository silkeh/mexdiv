#!/usr/bin/env python3
"""Mex Divider script."""

import copy
import logging
import math
import random
from typing import List, Tuple

from mex_divider.sup_com_map import Map, Player, Mex

starting_mexes = 0


def best_mex(
    army: Player, free_mexes: List[Mex], armies: List[Player], mexes: List[Mex]
) -> Tuple[int, float]:
    """Get the best mex."""
    my_mexes = []
    for i in army.mexes:
        my_mexes.append(mexes[i])
    distance_list = list(map(lambda mex: army.distanceTo(mex), free_mexes))
    if my_mexes:
        distance_owned = []
        for m in free_mexes:
            do = []
            for own in my_mexes:
                do.append(own.distanceTo(m))
            distance_owned.append(min(do))

    else:
        distance_owned = [0] * len(distance_list)

    distance_others = []
    for m in free_mexes:
        current_distance = 10000.0
        for other in armies:
            if other == army:
                continue
            other_mexes = []
            for i in other.mexes:
                other_mexes.append(mexes[i])
            if other_mexes:
                other_distance = []
                for other_mex in other_mexes:
                    other_distance.append(m.distanceTo(other_mex))
                min_other_distance = min(other_distance)
                current_distance = min(min_other_distance, current_distance)
        distance_others.append(current_distance)

    score = list(
        map(
            lambda d, o, od: math.sqrt((d * d + o * o)) + 50 / od,
            distance_list,
            distance_owned,
            distance_others,
        )
    )
    current_distance, mex = min((val, idx) for (idx, val) in enumerate(score))
    return mex, current_distance


def costs(army: Player, mexes: List[Mex]) -> float:
    """Get costs."""
    am = []
    for i in army.mexes:
        am.append(mexes[i])
    return sum(map(lambda m: pow(max(army.distanceTo(m), 0), 2), am))


def total_costs(armies: List[Player], mexes: List[Mex]) -> float:
    """Get total costs."""
    cl = list(map(lambda a: costs(a, mexes), armies))
    return sum(map(lambda c: math.pow(c, 2), cl))


# mypy: ignore-errors
def random_swap(armies: list[Player]):
    """Random swap."""
    victims = random.sample(armies, 2)
    l1 = victims[0].mexes
    l2 = victims[1].mexes
    e1 = random.choice(victims[0].mexes[starting_mexes:])
    e2 = random.choice(victims[1].mexes[starting_mexes:])
    l2[l2.index(e2)] = e1
    l1[l1.index(e1)] = e2
    pass


def anneal(armies: List[Player], t: int, mexes: List[Mex]) -> List[Player]:
    """Anneal."""
    logging.info(f"Old costs: {total_costs(armies, mexes)}")
    for _ in range(1000):
        n_armies = copy.deepcopy(armies)
        for _ in range(random.randint(1, t)):
            random_swap(n_armies)
        oc = total_costs(armies, mexes)
        nc = total_costs(n_armies, mexes)
        if nc < oc:
            armies = n_armies
        else:
            diff = nc - oc
            if random.random() < math.exp(-diff / t / 1000000):
                armies = n_armies
    logging.info(f"New costs: {total_costs(armies, mexes)}")
    return armies


def claim_mexes(armies, mexes):
    """Claim mexes."""
    free_mexes = mexes.copy()

    for army in armies:
        army.mexes = []

    for i in range(math.floor(len(mexes) / len(armies))):
        for army in armies:
            closest, score = best_mex(army, free_mexes, armies, mexes)
            army.mexes.append(free_mexes[closest].id)
            free_mexes.pop(closest)

    free_i = list(map(lambda m: m.id, free_mexes))

    army = armies[0]
    my_mexes = []
    for i in army.mexes:
        my_mexes.append(mexes[i])
    distance_list = list(map(lambda mex: army.distanceTo(mex), my_mexes))
    starting_mexes = sum(map(lambda d: d < 20, distance_list))

    return free_i, starting_mexes


def divide(local_map: Map):
    """Divide the mexes on the map between players.

    :param local_map: A sup com map where the mexes should be divided.
    """
    free_i, starting_mexes = claim_mexes(local_map.players, local_map.mexes)
    logging.info(f"Amount of starting mexes: {starting_mexes}")

    # Optimize mex distribution by swapping
    for t in range(5, 0, -1):
        local_map.players = anneal(local_map.players, t, local_map.mexes)
    for i, p in enumerate(local_map.players):
        for mex_id in p.mexes:
            local_map.mexes[mex_id].set_owner(p)
    local_map.divided = True


if __name__ == "__main__":
    map_seed = "neroxis_map_generator_1.7.1_tktdyh4g555zw_biiaeba_"
    loaded_map = Map(map_seed, map_directory="map")
    divide(loaded_map)
    loaded_map.save_to_json()
    print(loaded_map)
