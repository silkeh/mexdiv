"""Module for dividing mexes of a sup_com_map object."""
import asyncio
from typing import Optional

from mex_divider import sup_com_map, drawable_map
from mex_divider.sup_com_map import change_mex_owner


class MexAssignedMap(drawable_map.Map):
    """Generate a map and creates an initial assignment of all the mexes to players."""

    STARTING_CIRCLE = 25.0

    def __init__(self, seed: str, map_directory: str):
        """
        Instantiate a new map for assigning mass extractors to players.

        :param seed: seed of the SupCom Map.
        :param map_directory: directory to store the map.
        """
        super().__init__(seed, map_directory)

    def costs(self) -> float:
        """Sum all costs for each player its mexes."""
        return sum(map(lambda player: player.costs(), self.players))

    def divide(self):
        """Divide the mexes among players."""
        self._divide_starting_mexes()
        self._divide_other_mexes()
        self.divided = True

    def _divide_starting_mexes(self):
        for player in self.players:
            for mex in self.unassigned_mexes_iter():
                if not mex.is_within(player, self.STARTING_CIRCLE):
                    continue
                change_mex_owner(mex, player)

    def _divide_other_mexes(self):
        while self._assign_mex_to_poorest_player():
            pass

    def _assign_mex_to_poorest_player(self) -> bool:
        player = self._get_poorest_player()
        if player is None:
            return False
        cheapest_mex = self._get_cheapest_mex(player)
        if cheapest_mex is None:
            return False
        change_mex_owner(cheapest_mex, player)
        return True

    def _get_cheapest_mex(
        self, player: sup_com_map.Player
    ) -> Optional[sup_com_map.Mex]:
        cheap_mex = None
        cheapest = float("inf")
        for mex in self.unassigned_mexes_iter():
            cost = mex.distance_to(player)
            if cost < cheapest:
                cheapest = cost
                cheap_mex = mex
        return cheap_mex

    def _get_poorest_player(self) -> Optional[sup_com_map.Player]:
        poor_player: Optional[sup_com_map.Player] = None
        # Calculate worst travel distance.
        poor_score: float = self.map_dimension["size"]["x"] * self.mexes_per_player
        for player in self.players:
            if len(player.mexes) >= self.mexes_per_player:
                continue
            player_score = self.map_dimension["size"]["x"] * (
                self.mexes_per_player - len(player.mexes)
            ) - sum(map(lambda mex: player.distance_to(mex), player.mexes))
            if poor_score > player_score:
                poor_player = player
                poor_score = player_score
        return poor_player


if __name__ == "__main__":
    seeds = [
        "neroxis_map_generator_1.8.3_ha6odzxv42zse_ayeae_",
        "neroxis_map_generator_1.8.3_7ukthnka2s264_aycae_",
        "neroxis_map_generator_1.8.3_ptm5sh4pu6vs2_aycae_",
        "neroxis_map_generator_1.8.3_emmw4nz46rrn4_ayiae_",
        "neroxis_map_generator_1.8.3_flxt5vv773qdc_ayhae_",
    ]

    for seed in seeds:
        new_map = MexAssignedMap(seed, "../map")
        asyncio.run(new_map.load_map())
        new_map.divide()
        new_map.save_image("annotated.png")
        new_map.save_to_json()
