# MexDiv

An API and website for dividing mass extractors (mexes) in Supreme Commander.

## Installation

Installation can be done using Poetry:

```console
poetry install --only main --no-root
```

Alternatively, the following Debian packages can be used (tested on Debian Bullseye):

```text
python3-gunicorn python3-uvicorn python3-pillow python3-quart python3-yaml python3-jsonschema
```

Service files for running the application can be found in `dist/systemd`.

## Development

Use [Poetry][] following to install all development dependencies:

```console
poetry install --no-root
```

Run the linters using:

```console
poetry run ruff format .
poetry run flake8 .
poetry run mypy .
```

Run the application (for development only) using:

```console
poetry run ./flask_devel.py
```

[Poetry]: https://python-poetry.org
