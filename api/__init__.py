"""Quart wrappers to start the webserver."""
import logging
import os

import yaml
from quart import Quart, Response, app
from quart.helpers import send_from_directory

from api import mex, player, cors, map, socket
from api.util import MapIdConverter

CONFIG_FILENAME = "config.yaml"


def create_app(name) -> app.Quart:
    """Set up the quart web application.

    :param name: Name of the quart app.
    :return: The quart app
    """
    web_app = Quart(name, static_url_path="", static_folder="static")
    load_config(web_app)

    web_app.url_map.converters["map_id"] = MapIdConverter
    web_app.add_url_rule("/", view_func=index)
    web_app.before_request(cors.handle_preflight)
    web_app.after_request(cors.set_headers)
    web_app.register_blueprint(mex.mex_bp)
    web_app.register_blueprint(player.player_bp)
    web_app.register_blueprint(map.map_bp)
    web_app.register_blueprint(socket.websocket_bp)

    return web_app


async def index() -> Response:
    """Serve the index.html file."""
    return await send_from_directory("static", "index.html")


def load_config(web_app: app.Quart, filename: str = CONFIG_FILENAME) -> None:
    """Load the configuration for the quart app.

    The file is loaded from the CONFIGURATION_DIRECTORY environment variable. If the environment
    variable does not exist the file is loaded from the current working directory.

    :param web_app: The quart app to load the config for
    :param filename: The filename of the configuration file
    """
    config_directory = os.getenv("CONFIGURATION_DIRECTORY", default=os.getcwd())
    config_file = os.path.join(config_directory, filename)
    with open(config_file, "r") as f:
        try:
            config_data = yaml.load(f, Loader=yaml.Loader)
        except yaml.YAMLError as e:
            logging.critical(f"Unable to load config file at {config_file}: {e}")
            config_data = {}
    web_app.config.from_mapping(config_data)
