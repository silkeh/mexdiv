"""API functionality for websocket."""
import asyncio
import json

from quart import Blueprint, websocket
from typing import AsyncGenerator, Dict

websocket_bp = Blueprint("websocket", __name__)


class MapUpdateBroker(object):
    """Manage a simple broker to pass update notifications to all listening clients."""

    def __init__(self):
        """Instantiate a new broker."""
        self.connections: Dict[str, set] = {}

    def _get_or_create_from_seed(self, map_seed):
        if map_seed not in self.connections:
            self.connections[map_seed] = set()
        return self.connections[map_seed]

    async def publish_update(self, map_seed) -> None:
        """Publish an empty update to all subscribers."""
        if map_seed in self.connections:
            for conn in self.connections[map_seed]:
                await conn.put(None)

    async def subscribe(self, map_seed: str) -> AsyncGenerator[str, None]:
        """Subscribe on update notifications.

        :return: Yields a None on every update
        """
        conn: asyncio.Queue = asyncio.Queue()

        self._get_or_create_from_seed(map_seed).add(conn)
        try:
            while True:
                yield await conn.get()
        finally:
            self._get_or_create_from_seed(map_seed).remove(conn)


broker = MapUpdateBroker()


@websocket_bp.websocket("/api/v1/<string:map_id>/ws")
async def ws(map_id: str) -> None:
    """Websocket endpoint."""
    try:
        async for update in broker.subscribe(map_id):
            await websocket.send(json.dumps({"type": "update"}))
    finally:
        pass


async def notify_clients(map_seed: str) -> None:
    """Notify all websocket clients of new update."""
    await broker.publish_update(map_seed)
