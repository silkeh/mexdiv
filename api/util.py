"""Utility classes for the API."""
from werkzeug.routing import BaseConverter
from lib.map_generator import MapGenerator


class MapIdConverter(BaseConverter):
    """Converter to check map_ids in urls."""

    regex = MapGenerator.MAP_NAME_REGEX
