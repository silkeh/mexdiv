"""API functionality for players."""
from os import path
from typing import Dict

import quart
import yaml
from jsonschema import validate, exceptions
from quart import Blueprint, current_app, request

from api.socket import notify_clients
from mex_divider import sup_com_map

player_bp = Blueprint("player", __name__)


def player_schema() -> Dict:
    """Return dict object with Player Schema."""
    with open(path.join(path.dirname(__file__), "..", "openapi.yaml"), "r") as file:
        data = yaml.load(file, Loader=yaml.FullLoader)
        return data["components"]["schemas"]["player"]


@player_bp.route("/api/v1/<map_id:map_id>/players", methods=["GET"])
async def get_players(map_id: str):
    """List of player formatter for web requests.

    :param map_id: A map generator-based map ID
    :return: A list of players for this map
    """
    _map = sup_com_map.Map(map_id, current_app.config["MAP_DIRECTORY"])
    await _map.load_map()
    return quart.jsonify(_map.get_players_dict())


@player_bp.route("/api/v1/<map_id:map_id>/players/<int:player_id>", methods=["GET"])
async def get_player_by_id(map_id: str, player_id: int):
    """Player object formatter and updater for web requests.

    :param map_id: A map generator-based map ID
    :param player_id: ID of the player
    :return: Player object for this map
    """
    _map = sup_com_map.Map(map_id, current_app.config["MAP_DIRECTORY"])
    await _map.load_map()
    return quart.jsonify(_map.get_player_dict_by_id(player_id))


@player_bp.route("/api/v1/<map_id:map_id>/players/<int:player_id>", methods=["PUT"])
async def set_player_by_id(map_id: str, player_id: int):
    """Player object formatter and updater for web requests.

    :param map_id: A map generator-based map ID
    :param player_id: ID of the player
    :return: Player object for this map
    """
    player_data = await request.get_json()
    try:
        validate(player_data, player_schema())
    except exceptions.ValidationError or exceptions.SchemaError as n:
        return quart.jsonify(success=False, errors=n.message)
    _map = sup_com_map.Map(map_id, current_app.config["MAP_DIRECTORY"])
    await _map.load_map()
    player = _map.players[player_id]
    player.name = player_data["name"]
    player.color = player_data["color"]
    _map.save_to_json()
    await notify_clients(map_id)
    return quart.jsonify(success=True)
