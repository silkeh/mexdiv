"""API functionality for map."""

import base64

import quart
from quart import Blueprint, current_app

from mex_divider import sup_com_map
from mex_divider.mex_assigned_map import MexAssignedMap

map_bp = Blueprint("map", __name__)


async def __divided_map(map_id: str) -> sup_com_map.Map:
    """Get a generated and divided map based on the given ID.

    :param map_id: A map generator-based map ID
    :return: A JSON object describing the generated map.
    """
    assigned_map = MexAssignedMap(map_id, current_app.config["MAP_DIRECTORY"])
    await assigned_map.load_map()

    if not assigned_map.divided:
        assigned_map.divide()
        assigned_map.save_to_json()

    return assigned_map


@map_bp.route("/api/v1/<map_id:map_id>", methods=["GET"])
async def get_map_division(map_id: str):
    """JSON map data formatter for web request.

    See also the openapi.yaml specification.

    :param map_id: A map generator-based map ID
    :return: A JSON object describing the generated map.
    """
    _map = await __divided_map(map_id)

    with open(_map.preview_file(), "rb") as file:
        preview = base64.b64encode(file.read()).decode("ascii")

    return quart.jsonify(
        {
            "mexes": _map.get_mexes_dict(),
            "players": _map.get_players_dict(),
            "preview": "data:image/png;base64,{}".format(preview),
            "map": _map.get_map_dict(),
        }
    )


@map_bp.route("/api/v1/<map_id:map_id>/preview", methods=["GET"])
async def get_map_image(map_id: str):
    """Map image as PNG.

    :param map_id: A map generator-based map ID
    :return: A PNG of the map
    """
    _map = await __divided_map(map_id)
    with open(_map.preview_file(), "rb") as file:
        return quart.Response(file.read(), mimetype="image/png")


@map_bp.route("/api/v1/<map_id:map_id>/map", methods=["GET"])
async def get_map_info(map_id: str):
    """Map size information.

    :param map_id: A map generator-based map ID
    :return: json object with mapsize
    """
    _map = await __divided_map(map_id)
    return quart.jsonify(_map.get_map_dict())
