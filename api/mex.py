"""API functionality for mexes."""
from os import path
from typing import Dict

import quart
import yaml
from jsonschema import validate, exceptions
from quart import Blueprint, current_app, request

from api.socket import notify_clients
from mex_divider import sup_com_map
from mex_divider.sup_com_map import change_mex_owner

mex_bp = Blueprint("mex", __name__)


def mex_schema() -> Dict:
    """Return dict object with Mex Schema."""
    with open(path.join(path.dirname(__file__), "..", "openapi.yaml"), "r") as file:
        data = yaml.load(file, Loader=yaml.FullLoader)
        return data["components"]["schemas"]["mex"]


@mex_bp.route("/api/v1/<map_id:map_id>/mexes", methods=["GET"])
async def get_mexes(map_id: str):
    """Mex list formatter endpoint for web request.

    See also the openapi.yaml specification.

    :param map_id: A map generator-based map ID
    :return: A list of mexes
    """
    _map = sup_com_map.Map(map_id, current_app.config["MAP_DIRECTORY"])
    await _map.load_map()
    return quart.jsonify(_map.get_mexes_dict())


@mex_bp.route("/api/v1/<map_id:map_id>/mexes/<int:mex_id>", methods=["GET"])
async def get_mex_by_id(map_id: str, mex_id: int):
    """Single mex by id JSON object formatter and updater for GET requests.

    See also the openapi.yaml specification.

    :param map_id: A map generator-based map ID
    :param mex_id: A mex by id
    :return: The information of the specified mex
    """
    _map = sup_com_map.Map(map_id, current_app.config["MAP_DIRECTORY"])
    await _map.load_map()
    return quart.jsonify(_map.get_mex_dict_by_id(mex_id))


@mex_bp.route("/api/v1/<map_id:map_id>/mexes/<int:mex_id>", methods=["PUT"])
async def set_mex_by_id(map_id: str, mex_id: int):
    """Single mex by id JSON object formatter and updater for PUT requests.

    See also the openapi.yaml specification.

    :param map_id: A map generator-based map ID
    :param mex_id: A mex by id
    :return: The information of the specified mex
    """
    mex_data = await request.get_json()
    try:
        validate(mex_data, mex_schema())
    except exceptions.ValidationError or exceptions.SchemaError as n:
        return quart.jsonify(success=False, errors=n.message)

    new_owner_id = mex_data["player_id"]

    _map = sup_com_map.Map(map_id, current_app.config["MAP_DIRECTORY"])
    await _map.load_map()
    change_mex_owner(_map.mexes[mex_id], _map.players[new_owner_id])
    _map.save_to_json()
    await notify_clients(map_id)
    return quart.jsonify(success=True)
