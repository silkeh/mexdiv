"""CORS handling functionality."""

from typing import Optional
from quart import request, Response, Request


def handle_preflight() -> Optional[Response]:
    """
    Handle a request and set the CORS headers if required.

    See: https://developer.mozilla.org/en-US/docs/Glossary/Preflight_request

    :return: a preflight response if the given request is a CORS preflight request,
             or None otherwise (letting the regular request continue).
    """
    if __is_preflight(request):
        return set_preflight_headers(Response(status=204))

    return None


def __is_preflight(req: Request) -> bool:
    return req.method == "OPTIONS" and "Access-Control-Request-Method" in req.headers


def set_headers(response: Response) -> Response:
    """
    Set CORS headers for a response.

    :param response: response to set headers on.
    """
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


def set_preflight_headers(response: Response) -> Response:
    """
    Set CORS headers for a preflight response.

    :param response: response to set headers on.
    """
    response.headers.add("Access-Control-Allow-Methods", "*")
    response.headers.add("Access-Control-Allow-Headers", "Content-Type")
    response.headers.add("Access-Control-Max-Age", "86400")
    return response
