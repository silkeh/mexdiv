#!/usr/bin/env python
# encoding: utf-8
"""Easy to use Quart development server."""
import logging

from application import app

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    app.run()
