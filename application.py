"""Initialize and start the quart app.

Can be used with gunicorn with:
```
gunicorn --threads 4 --bind 0.0.0.0:5000 application:app
    -k uvicorn.workers.UvicornWorker
```
"""
from api import create_app

app = create_app(__name__)
