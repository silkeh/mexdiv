export class Point {
    x = 0;

    y = 0;

    constructor (x, y) {
        this.x = x;
        this.y = y;
    }

    copy () {
        return new Point(
            this.x,
            this.y
        );
    }

    add (other) {
        this.x += other.x;
        this.y += other.y;

        return this;
    }

    subtract (other) {
        this.x -= other.x;
        this.y -= other.y;

        return this;
    }

    multiply (factor) {
        this.x *= factor;
        this.y *= factor;

        return this;
    }

    divide (factor) {
        this.x /= factor;
        this.y /= factor;

        return this;
    }

    round () {
        this.x = Math.round(this.x);
        this.y = Math.round(this.y);

        return this;
    }

    floor () {
        this.x = Math.floor(this.x);
        this.y = Math.floor(this.y);

        return this;
    }

    ceil () {
        this.x = Math.ceil(this.x);
        this.y = Math.ceil(this.y);

        return this;
    }
}
