import { Layer } from "./layer.js";

export class BackgroundLayer extends Layer {
    constructor (width, height, asyncDrawContext, canvas = document.createElement("canvas")) {
        super(
            width,
            height,
            asyncDrawContext,
            canvas
        );
        this.image = new Image();
        this.renderedPreview = "";
        this.image.onload = this.imageLoaded;
        BackgroundLayer.loadedEvent = new CustomEvent("backgroundImageLoaded");
    }

    draw (preview) {
        if (this.renderedPreview === preview) {
            try {
                this.ctx.drawImage(
                    this.image,
                    0,
                    0
                );
            } catch (err) {
                console.log("Background image failed to draw. You can ignore on empty map.");
            }
        } else {
            this.renderedPreview = preview;
            this.image.src = preview;
        }
    }

    imageLoaded () {
        window.dispatchEvent(BackgroundLayer.loadedEvent);
    }
}
