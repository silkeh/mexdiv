import {Layer} from "./layer.js";
import {Voronoi} from "./rhill-voronoi-core.js";

export class DivisionLayer extends Layer {
    highlight = "white";

    draw (players, mexes, mapSize) {
        this.calculateVoronoi(mexes, mapSize);
        const ctx = this.canvas.getContext("2d");
        const cells = this.diagram.cells;

        for (const siteId in this.sites) {
            const site = this.sites[siteId];
            const cell = cells[site.voronoiId];
            const mex = mexes[site.mexid];
            const owner = mex.player_id;

            ctx.beginPath();
            ctx.fillStyle = players[owner].color + "80";
            const point = cell.halfedges[0].getStartpoint();
            ctx.moveTo(point.x / this.voronoiScale, point.y / this.voronoiScale);
            for (const iEdge in cell.halfedges) {
                const point = cell.halfedges[iEdge].getEndpoint();
                ctx.lineTo(point.x / this.voronoiScale, point.y / this.voronoiScale);
            }
            ctx.closePath();

            if (mex.selected) {
                ctx.strokeStyle = this.highlight;
                ctx.stroke();
            }

            ctx.fill();
        }
    }

    calculateVoronoi (mexes, mapSize) {
        this.voronoi = new Voronoi();
        this.sites = [];
        let max = 0.0;

        for (const mex in mexes) {
            this.sites.push({ x: mexes[mex].location.x, y: mexes[mex].location.y, mexid: mex });
            max = Math.max(mexes[mex].location.x, max = mexes[mex].location.y, max);
        }

        this.voronoiScale = 2;
        this.diagram = this.voronoi.compute(this.sites, { xl: 0, xr: mapSize * 2, yt: 0, yb: mapSize * 2 });
    }
}
