import { Model } from "./model.js";
import { ViewModel } from "./viewModel.js";

window.init = () => {
    window.model = new Model(document.getElementById("mapIdInput").value);
    window.viewModel = new ViewModel();
    if (window.location.search) {
        document.getElementById("mapIdInput").value = window.location.search.substring(1);
    }
    window.updateId(window.model);
};

window.updateId = () => {
    window.viewModel.resetView();
    window.model.load(document.getElementById("mapIdInput").value);
    window.viewModel.update(window.model);
};
document.getElementById("mapIdInputButton").addEventListener(
    "click",
    window.updateId
);

window.resetView = () => {
    window.viewModel.resetView();
};
document.getElementById("resetViewButton").addEventListener(
    "click",
    window.resetView
);
