export class Layer {
    constructor (width, height, asyncDrawContext, canvas = document.createElement("canvas")) {
        this.canvas = canvas;
        this.canvas.width = width;
        this.canvas.height = height;
        this.ctx = this.canvas.getContext("2d");
        this.asyncDrawContext = asyncDrawContext;
    }

    clearAndTransform (scale, leftTopCoordinate) {
        this.clear();
        this.transform(
            scale,
            leftTopCoordinate
        );
    }

    clear () {
        this.ctx.resetTransform();
        // Clear entire screen, must be done before translation and scale
        this.ctx.fillStyle = "white";
        this.ctx.clearRect(
            0,
            0,
            this.canvas.width,
            this.canvas.height
        );
    }

    transform (scale, leftTopCoordinate) {
        this.ctx.resetTransform();
        const centerUnitPoint = leftTopCoordinate.copy();
        this.ctx.scale(
            scale,
            scale
        );
        this.ctx.translate(
            -centerUnitPoint.x,
            -centerUnitPoint.y
        );
    }
}
