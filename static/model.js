function distance (x1, y1, x2, y2) {
    return Math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2);
}

export class Model {
    baseurl = "/api/v1/";

    constructor (url) {
        this.url = url;
        this.websocket = null;
        this.manageWebSocket();
        this.reload();
    }

    manageWebSocket () {
        if (this.url.length === 0) {
            return;
        }

        var loc = window.location;
        var ws_uri;
        if (loc.protocol === "https:") {
            ws_uri = "wss:";
        } else {
            ws_uri = "ws:";
        }
        ws_uri += "//" + loc.host;
        ws_uri += this.baseurl + this.url + "/ws";

        if (this.websocket !== null) {
            if (this.websocket.url === ws_uri) {
                return;
            } else {
                this.websocket.close();
            }
        }

        this.websocket = new WebSocket(ws_uri);
        this.websocket.onmessage = (event) => {
            const message = JSON.parse(event.data);
            if (message.type === "update"){
                this.reload();
            }
        };
    }

    load (url) {
        this.url = url;
        this.manageWebSocket();
        this.reload();
    }

    async reload () {
        if (this.url.length === 0) {
            return;
        }

        fetch(`${this.baseurl}${this.url}`).
            then((response) => {
                this.processMapResponse(response);
            });
    }

    processMapResponse(response){
        if (response.ok){
            response.json().then((data) =>{
                this.map = data.map;
                this.mexes = data.mexes;
                this.players = data.players;
                this.preview = data.preview;
                this.mapSize = Math.max(data.map.size.x, data.map.size.y);
            }).then(window.viewModel.update());
        } else {
            console.log("Invalid map id");
        }
    }

    swapMex (mexA, mexB) {
        const playerA = mexA.player_id;
        const playerB = mexB.player_id;
        this.updateMexOwner(mexA, playerB).then(() =>
            this.updateMexOwner(mexB, playerA)
        ).then(() =>
            this.reload()
        );
    }

    assignMex (mex, player_id) {
        this.updateMexOwner(mex, player_id).
            then(() =>
                this.reload()
            );
    }

    updateMexOwner (mex, player_id) {
        return fetch(`${this.baseurl}${this.url}/mexes/${mex.id}`,
            {
                method: "PUT",
                headers: { "content-type": "application/json" },
                body: JSON.stringify({
                    player_id: player_id
                })
            }
        );
    }

    closestMex (x, y) {
        return this.mexes.reduce(function (prev, curr) {
            return distance(
                curr.location.x,
                curr.location.y,
                x,
                y
            ) < distance(
                prev.location.x,
                prev.location.y,
                x,
                y
            )
                ? curr
                : prev;
        });
    }

    changePlayer (id, name, color) {
        fetch(`${this.baseurl}${this.url}/players/${id}`,
            {
                method: "PUT",
                headers: { "content-type": "application/json" },
                body: JSON.stringify({
                    name: name,
                    color: color
                })
            }).then(() => this.reload());
    }
}
