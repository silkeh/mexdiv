import { Point } from "./point.js";
import { ParentLayer } from "./parentLayer.js";
import { BackgroundLayer } from "./backgroundLayer.js";
import { DivisionLayer } from "./divisionLayer.js";

export class Canvas {
    layers;

    defaultScale = 2.0;

    constructor (id, updateCallback) {
        this.canvas = document.getElementById(id);
        this.ctx = this.canvas.getContext("2d");
        this.updateCallback = updateCallback;

        // Scale is in units per pixel, so a scale smaller than 1 means smaller drawings
        this.scale = this.defaultScale;
        this.scaleMax = 5;
        this.scaleMin = 2;
        this.canvasWidth = 512;
        // LeftTopCoordinate is the point viewed at the top left corner of the canvas, expressed in unit coordinates
        this.leftTopCoordinate = new Point(
            0,
            0
        );

        this.parentLayer = new ParentLayer(
            this.canvas.width,
            this.canvas.height,
            null,
            this.canvas
        );

        this.backgroundLayer = new BackgroundLayer(
            this.canvas.width,
            this.canvas.height,
            this.parentLayer.ctx
        );
        this.divisionLayer = new DivisionLayer(
            this.canvas.width,
            this.canvas.height,
            this.parentLayer.ctx
        );

        this.layers = new Set();
        this.layers.add(this.backgroundLayer);
        this.layers.add(this.divisionLayer);

        this.controlDown = false;
        this.mouseDownCoordinate = null;
        this.mouseCoordinate = null;

        this.canvas.addEventListener(
            "mousedown",
            (event) => this.mouseDown(event)
        );
        this.canvas.addEventListener(
            "mouseup",
            (event) => this.mouseUp(event)
        );
        this.canvas.addEventListener(
            "mousemove",
            (event) => this.mouseMove(event)
        );
        this.canvas.addEventListener(
            "mousewheel",
            (event) => this.mouseScroll(event)
        );
        this.canvas.addEventListener(
            "DOMMouseScroll",
            (event) => this.mouseScroll(event)
        );
        this.canvas.addEventListener(
            "mouseleave",
            (event) => this.mouseLeave(event)
        );

        window.addEventListener(
            "backgroundImageLoaded",
            () => this.updateCallback()
        );
    }

    clampView () {
        if (this.leftTopCoordinate.x < 0) {
            this.leftTopCoordinate.x = 0;
        }
        if (this.leftTopCoordinate.y < 0) {
            this.leftTopCoordinate.y = 0;
        }
        const bottomRightCoordinateMax = 256 - this.canvasWidth / this.scale;
        if (this.leftTopCoordinate.x > bottomRightCoordinateMax) {
            this.leftTopCoordinate.x = bottomRightCoordinateMax;
        }
        if (this.leftTopCoordinate.y > bottomRightCoordinateMax) {
            this.leftTopCoordinate.y = bottomRightCoordinateMax;
        }
    }

    mouseDown (event) {
        this.mouseDownCoordinate = this.coordinateAt(new Point(
            event.offsetX,
            event.offsetY
        ));
        this.mouseEvent(event);
        event.down = true;
        if (event.middle) {
            event.preventDefault();
        }
        this.updateCallback(event);
    }

    mouseUp (event) {
        this.mouseCoordinate = this.coordinateAt(new Point(
            event.offsetX,
            event.offsetY
        ));
        this.mouseEvent(event);
        event.up = true;
        this.mouseDownCoordinate = null;
        this.updateCallback(event);
    }

    mouseLeave (event) {
        this.mouseDownCoordinate = null;
        this.mouseCoordinate = null;
        this.mouseEvent(event);
        event.leave = true;
        this.updateCallback(event);
    }

    mouseMove (event) {
        this.mouseEvent(event);
        if (event.buttons & 4) {
            const movement = new Point(
                event.movementX,
                event.movementY
            ).divide(this.scale);
            this.leftTopCoordinate.subtract(movement);
            this.clampView();
            this.updateCallback({ update: true });

            return;
        }
        event.move = true;
        this.updateCallback(event);
    }

    mouseScroll (event) {
        const delta = event.wheelDelta
            ? event.wheelDelta / 40
            : event.detail
                ? -event.detail
                : 0;
        if (delta && !(event.buttons & 4)) {
            let scaleFactor = delta > 0
                ? 1.1 ** delta
                : (1 / 1.1) ** -delta;
            let newScale = this.scale * scaleFactor;
            if (newScale > this.scaleMax) {
                newScale = this.scaleMax;
                scaleFactor = newScale / this.scale;
            } else if (newScale < this.scaleMin) {
                newScale = this.scaleMin;
                scaleFactor = newScale / this.scale;
            }
            // When zooming, the coordinate under the mouse should stay the same after the zoom
            const coordinateMousePointsTo = this.coordinateAt(new Point(
                event.offsetX,
                event.offsetY
            ));
            const distanceToLeftTop = this.leftTopCoordinate.copy().subtract(coordinateMousePointsTo);
            const diffDistanceToLeftTop = distanceToLeftTop.copy().multiply(1 - 1 / scaleFactor);
            // Calculate new center and zoom
            this.scale = newScale;
            this.leftTopCoordinate.subtract(diffDistanceToLeftTop);
            this.clampView();
            this.updateCallback({ update: true });
        }
        event.preventDefault();
    }

    mouseEvent (event) {
        this.mouseCoordinate = this.coordinateAt(new Point(
            event.offsetX,
            event.offsetY
        ));
        event.point1 = this.mouseDownCoordinate;
        event.point2 = this.mouseCoordinate;
        if (event.button === 0) { // Left mouse
            event.left = true;
        } else if (event.button === 1) { // Middle mouse
            event.middle = true;
        } else if (event.button === 2) { // Right Mouse
            event.right = true;
        }
    }

    resetView () {
        this.leftTopCoordinate = new Point(
            0,
            0
        );
        this.scale = this.defaultScale;
        this.updateCallback({ update: true });
    }

    redraw (model) {
        this.clearAll();
        this.backgroundLayer.draw(model.preview);
        this.divisionLayer.draw(
            model.players,
            model.mexes,
            model.mapSize
        );
        this.drawParentLayer();
    }

    drawParentLayer () {
        this.parentLayer.draw(this.layers);
        // Prepare the parent layer for any asynchronous draws
        this.parentLayer.transform(
            this.scale,
            this.leftTopCoordinate
        );
    }

    clearAll () {
        this.parentLayer.clear();
        for (const layer of this.layers) {
            layer.clearAndTransform(
                this.scale,
                this.leftTopCoordinate
            );
        }
    }

    coordinateAt (pixel) {
        return new Point(
            pixel.x / this.scale + this.leftTopCoordinate.x,
            pixel.y / this.scale + this.leftTopCoordinate.y
        );
    }

    pixelAt (coordinate) {
        return new Point(
            (coordinate.x - this.leftTopCoordinate.x) * this.scale,
            (coordinate.y - this.leftTopCoordinate.y) * this.scale
        );
    }
}
