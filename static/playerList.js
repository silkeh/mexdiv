export class PlayerList {
    constructor (id, playerChangeCallback, mexAssignCallback) {
        this.playerListElement = document.getElementById(id);
        this.playerChangeCallback = playerChangeCallback;
        this.mexAssignCallback = mexAssignCallback;
    }

    update(model){
        if (model.players){
            this.playerListElement.innerHTML = "";
            for (let player in model.players){
                let playerEntry = this.createEntryForPlayer(model.players[player]);
                this.playerListElement.appendChild(playerEntry);
            }
        }
    }

    playerColorEvent(event){
        this.playerChangeCallback(this.playerEvent(event.target.id, null, event.target.value));
    }

    playerNameEvent(event){
        this.playerChangeCallback(this.playerEvent(event.target.id, event.target.value, null));
    }

    mexAssignEvent(event) {
        this.mexAssignCallback({player_id:event.target.id});
    }

    playerEvent(id, name, color){
        return {id:id, name:name, color:color};
    }

    createEntryForPlayer(player){
        let playerName = player.name;
        if (playerName === "") {
            playerName = "slot " + player.id;
        }

        let playerNameInput = document.createElement("input");
        playerNameInput.setAttribute("value", playerName);
        playerNameInput.id = player.id;
        playerNameInput.size = 8;
        playerNameInput.onchange =(event) => this.playerNameEvent(event);

        let playerColorPicker = document.createElement("input");
        playerColorPicker.type = "color";
        playerColorPicker.onchange = (event) => this.playerColorEvent(event);
        playerColorPicker.id = player.id;
        playerColorPicker.value = player.color;

        let playerAssignButton = document.createElement("button");
        playerAssignButton.innerText = "Assign";
        playerAssignButton.onclick = (event) => this.mexAssignEvent(event);
        playerAssignButton.id = player.id;

        let playerEntry = document.createElement("div");
        playerEntry.appendChild(playerNameInput);
        playerEntry.appendChild(playerColorPicker);
        playerEntry.appendChild(playerAssignButton);

        let listItem = document.createElement("li");
        listItem.className = "playerEntry";
        listItem.appendChild(playerEntry);

        return listItem;
    }
}
