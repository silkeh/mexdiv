import { Layer } from "./layer.js";

export class ParentLayer extends Layer {
    draw (layers) {
        this.ctx.resetTransform();
        for (const layer of layers) {
            this.ctx.drawImage(
                layer.canvas,
                0,
                0
            );
        }
    }
}
