import {Canvas} from "./canvas.js";
import {PlayerList} from "./playerList.js";

export class ViewModel {
    constructor () {
        this.canvas = new Canvas(
            "grid",
            (event) => this.updateEvent(event)
        );

        this.playerList = new PlayerList(
            "playerList",
            (event) => this.playerChange(event),
            (event) => this.mexAssign(event)
        );

        this.model = window.model;
        this.selected = null;
    }

    resetView () {
        this.canvas.resetView();
    }

    screenToWorld (point) {
        return point.multiply(this.model.map.size.x / 256);
    }

    worldToScreen (point) {
        return point.divide(this.model.map.size.x / 256);
    }

    focusOut (event) {
        if (event.target.id === "grid") {
            this.canvas.mouseLeave(event);
        }
    }

    /* eslint no-unused-vars: off */
    updateEvent (event) {
        if (event === undefined) {
            this.update();

            return;
        }
        if (event.type === "mouseup" && event.left) {
            const startLocation = this.screenToWorld(event.point1);
            const originalMex = this.model.closestMex(startLocation.x, startLocation.y);
            const endLocation = this.screenToWorld(event.point2);
            const targetMex = this.model.closestMex(endLocation.x, endLocation.y);
            if (originalMex === targetMex) {
                if (this.selected) {
                    this.selected.selected = false;
                }
                this.selected = targetMex;
                this.selected.selected = true;
                console.log(`Selected: ${this.selected.id}`);
            } else {
                this.model.swapMex(originalMex, targetMex);
                console.log(`Swapped ${targetMex.id} and ${originalMex.id}`);
            }
        }
        this.update();
    }

    mexAssign (event) {
        if (this.selected) {
            this.model.assignMex(this.selected, parseInt(event.player_id));
        }
    }

    playerChange (event) {
        if (event.color === null) {
            event.color = this.model.players[event.id].color;
        }

        if (event.name === null) {
            event.name = this.model.players[event.id].name;
        }

        this.model.changePlayer(event.id, event.name, event.color);
    }

    update (model = window.model) {
        this.canvas.redraw(model);
        this.playerList.update(model);
    }
}
